import React from 'react';
import './Footer.css';
const Footer=props=>{
    return (<footer className="footer">
        <div className="container footer__foot-box">
            <div className="footer__box1">
                <p className="footer__text">
                    {props.compInfo}
                </p>

            </div>

            <div className="footer__box3">
                <p className="footer__text footer__text--for-logo">
                    {props.compName}
                </p>

            </div>

        </div>
    </footer>);
};
export default Footer;
