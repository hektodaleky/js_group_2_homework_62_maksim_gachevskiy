import React from "react";
import './Info.css';
const Info = props => {
    return (<div className="Info">
        <h2>{props.title}</h2>
        <div>
            <p>
                {props.text}
            </p>
        </div>
    </div>)
};
export default Info;