import React from 'react';
import './Header.css';
const Header=props=>{
    return (
        <header className="header">
            <div className="container">
                <a className="header__logo" href="#">
                    <img className="lg" src="" alt="">{props.image}</img>
                </a>
                <div className="header__contact-div">
                    <p className="header__contact-number">{props.phone1}</p>
                    <p className="header__contact-number">{props.phone2}</p>
                    <a className="header__contact-return show-btn" href=""
                       >Обратная связь</a>
                </div>

            </div>
        </header>)

};
export default Header;