import React from "react";
import Info from "../../components/Info/Info";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
const myText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. " +
    "Animi consequatur cumque fugit nostrum optio! Asperiores cumque debitis doloremque earum " +
    "facilis nesciunt odio perspiciatis quam, qui recusandae sit soluta sunt voluptates? " +
    "Lorem ipsum dolor sandae sit soluta sunt voluptates? " +
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. " +
    "Animi consequatur cumque fugit nostrum optio! Asperiores cumque debitis doloremque earum " +
    "facilis nesciunt odio perspiciatis quam, qui recusandae sit soluta sunt voluptates?";
const Contact = props => {

    return (<div>
        <Header phone1="+7(499) 777-77-77" phone2="+7(499) 777-11-77"/>


        <Info title="Our email mail@mail.mail" text={myText}></Info>
        <Footer compInfo="SuperCompany" compName="SuperSuperCompany"/>


    </div>)
};
export default Contact;