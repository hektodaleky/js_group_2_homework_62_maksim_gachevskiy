import React, {Component, Fragment} from "react";
import "./Menu.css";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Info from "../../components/Info/Info";
import {NavLink} from "react-router-dom";
const myText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. " +
    "Animi consequatur cumque fugit nostrum optio! Asperiores cumque debitis doloremque earum " +
    "facilis nesciunt odio perspiciatis quam, qui recusandae sit soluta sunt voluptates? " +
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. " +
    "Animi consequatur cumque fugit nostrum optio! Asperiores cumque debitis doloremque earum " +
    "facilis nesciunt odio perspiciatis quam, qui recusandae sit soluta sunt voluptates? " +
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. " +
    "Animi consequatur cumque fugit nostrum optio! Asperiores cumque debitis doloremque earum " +
    "facilis nesciunt odio perspiciatis quam, qui recusandae sit soluta sunt voluptates?";
class Menu extends Component {
    state = {
        items: ['Home', 'AboutUs', 'Contact']
    };
    followLink = name => {
        this.props.history.push({
            pathname: `/${name}`
        });

        this.props.history.replace(`/${name.toLowerCase().replace(' ', '')}`);
    };

    render() {
        return (
            <Fragment>
                <Header phone1="+7(499) 777-77-77" phone2="+7(499) 777-11-77"/>

                <ul className="Menu">
                    {
                        this.state.items.map((item, index) => <li className="Item" key={index}><NavLink

                            to={item}>{item}</NavLink></li>)
                    }
                </ul>
                <Info text={myText}/>
                <Footer compInfo="SuperCompany" compName="SuperSuperCompany"/>
            </Fragment>)
    }
}

export default Menu;