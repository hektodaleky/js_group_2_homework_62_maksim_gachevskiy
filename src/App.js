import React, { Component } from 'react';
import './App.css';
import {Switch,Route} from "react-router-dom";
import Menu from "./container/Menu/Menu";
import Home from "./container/Home/Home";
import AboutUs from "./container/About/AboutUs";
import Contact from "./container/Contact/Contact";

class App extends Component {
    render() {
        return (
            <Switch>
              <Route path="/home" component={Home}/>
              <Route path="/aboutus" component={AboutUs}/>
              <Route path="/contact" component={Contact}/>
              <Route path="/" exact component={Menu}/>
              <Route render={()=><h1>Not Found</h1>}/>
            </Switch>

        );
    }
}

export default App;
